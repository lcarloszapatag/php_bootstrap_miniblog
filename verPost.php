<?php
session_start();
include 'recursos.php';
include 'SQL.php';
$conn = new MySQL();

$post = $conn->getPostEspecifico($_GET['id']);
$usuarioPost = $conn->getUserPost($_GET['id']);
$usuario = $conn->getUser($usuarioPost);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/index.css">
    <title>Blog del curso</title>
  </head>
  <body>

    <!-- Barra de navegacion -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">
            <img src="img/php.png" alt="Brand"  style="height:40px" />
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php" >Incio</a></li>
            <li><a href="perfil.php?id=<?php echo $_SESSION['user'];?>" >Perfil<div></a></li>
            <li><a href="nuevoPost.php" >Nuevo</a></li>
            <li><a href="unlog.php" class="last">Cerrar sesión</a></li>
          </ul>
          <form class="navbar-form navbar-right" action="searchUser.php" method="get">
            <input type="text" name="s" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <br><br><br><br>
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <?php echo $conn->getPostEspecifico($_GET['id']);?>
          <div class="row">
            <div class="row panel">
              <div class="panel-heading">
                <h3 class="text-primary"><?php echo $conn->getTotalComentario($_GET['id']); ?> Comentarios</h3>
              </div>
              <div class="panel-body">
                <?php echo $conn->getComentarios($_GET['id']); ?>
              </div>
              <div class="panel-footer">
                <form class="form" action="comentar.php" method="post">
                  <textarea name="contenido" rows="3" class="form-control" placeholder="Ingresa un comentario....."></textarea>
                  <input type="hidden" name="post" value=<?php echo "'".$_GET['id']."'"; ?> >
                  <input type="submit" class="form-control btn btn-success" value="Comentar" >
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Comentarios -->


        <!-- Sidebar-->
        <div class="col-sm-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Entradas más comentadas
            </div>

            <div class="panel-body">
              <?php echo $conn->getMejoresPost(); ?>
            </div>

          </div>

          <div class="panel panel-primary">
            <div class="panel-heading">
              Acerca de
            </div>
            <div class="panel-body">
              <div class="col-sm-4">
                <img  class="img-responsive img-circle" src="img/usuario.jpg" alt="Responsive-img" />
              </div>
              <div class="col-sm-8">
                <a href="perfil.php?id=<?php echo $usuario['username'];?>"><h4>@<?php echo $usuario['username']; ?></h4></a>
                <h5 class="text-muted"><?php echo $usuario['nombre']." ".$usuario['apellidos'];?></h5>
                <h5>Contacto: <?php echo $usuario['correo'];?></h5>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>


    </div>






    <!-- NEcesario para bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
