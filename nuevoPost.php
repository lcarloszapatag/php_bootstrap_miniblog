<?php
session_start();
include 'recursos.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/index.css">
    <title>Blog del curso</title>
  </head>
  <body>

    
    <!-- Barra de navegacion -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">
            <img src="img/php.png" alt="Brand"  style="height:40px" />
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php" >Incio</a></li>
            <li><a href="perfil.php?id=<?php echo $_SESSION['user'];?>" >Perfil<div></a></li>
            <li><a href="nuevoPost.php" >Nuevo</a></li>
            <li><a href="unlog.php" class="last">Cerrar sesión</a></li>
          </ul>
          <form class="navbar-form navbar-right" action="searchUser.php" method="get">
            <input type="text" name="s" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <br><br><br><br>
    <div class="container" >
      <div class="row">
        <div class="col-sm-8">
          <form class="form-horizontal" action="publicar.php" method="post">
            <div class="form-group">
              <label for="titulo">Titulo</label>
              <input class="form-control" type="text" name="titulo" placeholder="Inserta aqui el titulo...">
            </div>
            <div class="form-group">
              <textarea class="form-control" name="contenido" rows="20" placeholder="Ingresa tu contenido..." ></textarea>
            </div>
            <div class="form-grup">
              <button type="submit" class="btn btn-success round">Aceptar</button>
              <a href="index.php" class="btn btn-danger round" >Cancelar</a>
            </div>
          </form>
        </div>


        <div class="col-sm-4 panel">
          <div>
            <div class="col-sm-4">
              <img src="img/usuario.jpg" class="img-responsive img-circle" alt="Responsive-img" />
            </div>
            <div class="col-sm-8">
              <h3>Publicado por @<?php echo $_SESSION['user']; ?></h3>
            </div>
          </div>

          <h3 class="text-muted"><small><?php echo date("d/m/Y"); ?></small></h3>
          <p>
  <pre>
  Todos los derechos reservados. 
  BLOG DEL CURSO PHP. 
  Este material es propiedad de @<?php echo $_SESSION['user']; ?>
  </pre>
          </p>
        </div>
      </div>
    </div>


    <!-- NEcesario para bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
