<?php
session_start();
include 'recursos.php';
include 'SQL.php';
$conn = new MySQL();
$usuario = $conn->getUser($_SESSION['user']);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/index.css">
    <title>Blog del curso</title>
    <style media="screen">
      .sidePanelCols {
        margin: 2px;
        padding: 2px;
      }
    </style>
  </head>
  <body>

    <!-- Barra de navegacion -->
    <!-- Barra de navegacion -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">
            <img src="img/php.png" alt="Brand"  style="height:40px" />
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php" >Incio</a></li>
            <li><a href="perfil.php?id=<?php echo $_SESSION['user'];?>" >Perfil<div></a></li>
            <li><a href="nuevoPost.php" >Nuevo</a></li>
            <li><a href="unlog.php" class="last">Cerrar sesión</a></li>
          </ul>
          <form class="navbar-form navbar-right" action="searchUser.php" method="get">
            <input type="text" name="s" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <br><br><br><br>
    <div class="container">

      <div class="row">

        <div class="col-sm-3 ">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Aceptar Follower
            </div>
            <?php echo $conn->getSolicitudes($_SESSION['user']);?>
          </div>

        </div>
        <div class="panel col-sm-6">  
          <?php 
            echo "<h1>Bienvenido @".$_SESSION['user']."</h1>";
            echo $conn->getPosts($_SESSION['user']); 
          ?>  
          <!-- Paginacion -->

          <nav>
            <ul class="pager">
              <li><a href="index.php?p=2&s=asdf">Previous</a></li>
              <li ><a href="index.php?p=1&s=asd">Next</a></li>
            </ul>
          </nav>

        </div>


        <div class="col-sm-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Entradas más comentadas
            </div>

            <div class="panel-body">
              <?php echo $conn->getMejoresPost(); ?>
            </div>

          </div>

          <div class="panel panel-primary">
            <div class="panel-heading">
              Acerca de
            </div>
            <div class="panel-body">
              <div class="col-sm-4">
                <img  class="img-responsive img-circle" src="img/usuario.jpg" alt="Responsive-img" />
              </div>
              <div class="col-sm-8">
                <a href="perfil.php?id=<?php echo $_SESSION['user'];?>"><h4>@<?php echo $_SESSION['user'];?></h4></a>
                <h5 class="text-muted"><?php echo $usuario['nombre']." ".$usuario['apellidos'];?></h5>
                <h5>Contacto: <?php echo $usuario['correo'];?></h5>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>








    <!-- NEcesario para bootstrap-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
